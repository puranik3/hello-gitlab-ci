var path = require( 'path' );

module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        
        vulcanize: {
            default: {
                options: {
                    // Task-specific options go here. 
                },
                files: {
                    // Target-specific file lists and/or options go here. 
                    'build.html': 'index.html'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-vulcanize');

    grunt.registerTask('default', ['uglify', 'cssmin', 'jscs:all']);
};